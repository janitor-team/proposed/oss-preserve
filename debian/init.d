#!/bin/sh

### BEGIN INIT INFO
# Provides:          oss-preserve
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

# $Id: init.d 2966 2006-08-15 05:11:48Z cph $
#
# Copyright (c) 2001 Hewlett-Packard Company
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.

set -e

OSSCTL="/usr/lib/oss-preserve/ossctl"
FIXED="/etc/default/oss-preserve"
PREVIOUS="/var/lib/oss-preserve/settings"

[ -x "${OSSCTL}" ] || exit 0

case "$1" in
start)
    SETTINGS=""
    if [ -s "${FIXED}" ]; then
	SETTINGS="${FIXED}"
    elif [ -s "${PREVIOUS}" ]; then
	SETTINGS="${PREVIOUS}"
    fi
    if [ -n "${SETTINGS}" ]; then
	echo -n "Restoring OSS mixer settings..."
	"${OSSCTL}" restore < "${SETTINGS}"
	echo "done."
    fi
    ;;
stop)
    echo -n "Saving OSS mixer settings..."
    "${OSSCTL}" save > "${PREVIOUS}"
    echo "done."
    ;;
restart|force-reload)
    $0 stop
    $0 start
    ;;
status)
    # http://refspecs.freestandards.org/LSB_2.1.0/LSB-generic/LSB-generic/iniscrptact.html
    # does not really specify an exit code:
    exit 4
    ;;
*)
    echo "Usage: $0 {start|stop|restart|force-reload}"
    exit 1
    ;;
esac

exit 0
